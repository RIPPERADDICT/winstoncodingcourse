import React, { Component } from 'react';
import classnames from 'classnames';

export default class Setting extends Component{
    
    render(){
        const { settings } = this.props;

        return settings.map(setting => {
            return (
                <ul className="setting">   
                    <span className="text">
                        <strong>{setting.text}</strong>
                    </span>
                </ul>
            );
        });
    }
}