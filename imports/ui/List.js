import React, { Component } from "react";
import { Meteor } from "meteor/meteor";
import classnames from "classnames";
import { Tasks } from "../api/tasks.js";

import Task from "./Task.js";

export default class List extends Component {
  deleteThisTask() {
    Meteor.call("lists.remove", this.props.list._id);
  }

  togglePrivate() {
    Meteor.call(
      "lists.setPrivate",
      this.props.list._id,
      !this.props.list.private
    );
  }

  renderTasks() {
    let filteredTasks = Tasks.find({
      listId: this.props.list._id
    }).fetch();

    if (this.props.hideCompleted) {
      filteredTasks = filteredTasks.filter(task => !task.checked);
    }

    return filteredTasks.map(task => {
      const currentUserId =
        this.props.currentUser && this.props.currentUser._id;
      const showPrivateButton = this.userId === currentUserId;

      return (
        <Task
          key={task._id}
          task={task}
          showPrivateButton={showPrivateButton}
        />
      );
    });
  }

  render() {
    // Give lists a different className when they are checked off,
    // so that we can style them nicely in CSS
    const listClassName = classnames({
      private: this.props.list.private
    });

    return (
      <ul className={listClassName}>
        <button className="delete" onClick={this.deleteThisTask.bind(this)}>
          &times;
        </button>

        <button
          className="toggle-private"
          onClick={this.togglePrivate.bind(this)}
        >
          {this.props.list.private ? "Make list Private" : "Make list Public"}
        </button>

        <span className="text">
          <strong>{this.props.list.name}</strong>:{this.props.list.username}
        </span>
        {this.renderTasks()}
      </ul>
    );
  }
}
