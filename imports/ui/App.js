import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";

import { Tasks } from "../api/tasks.js";
import { Lists } from "../api/lists.js";
import { Settings } from "../api/settings";

import List from "./List.js";
import Setting from "./Setting.js";
import AccountsUIWrapper from "./AccountsUIWrapper.js";

// App component - represents the whole app
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hideCompleted: false,
      hideForm: true,
      hideMenu: true
    };
  }

  addList(event) {
    //   console.log(event, "test");
    event.preventDefault();

    // Find the text field via the React ref
    const text = ReactDOM.findDOMNode(this.refs.listInput).value.trim();

    Meteor.call("lists.insert", text);

    // Clear form
    ReactDOM.findDOMNode(this.refs.listInput).value = "";
  }

  addTask(event) {
    //  console.log(event, "test");
    event.preventDefault();

    const text = ReactDOM.findDOMNode(this.refs.taskInput).value.trim();
    const listId = ReactDOM.findDOMNode(this.refs.listSelect).value.trim();
    // console.log(listId)

    Meteor.call("tasks.insert", text, listId);

    // Clear form
    ReactDOM.findDOMNode(this.refs.taskInput).value = "";
  }
  addSettings(event) {
    event.preventDefault();

    const text = ReactDOM.findDOMNode(this.refs.settingsInput).value.trim();

    Meteor.call("settings.insert", text);

    // Clear form
    ReactDOM.findDOMNode(this.refs.settingsInput).value = "";
  }
  toggleHideCompleted() {
    this.setState({
      hideCompleted: !this.state.hideCompleted
    });
  }

  // toggled zodat de state naar hide/show staat
  toggleHideForm() {
    this.setState({
      hideForm: !this.state.hideForm
    });
  }

  toggleMenu() {
    this.setState({
      hideMenu: !this.state.hideMenu
    });
  }

  renderLists() {
    const { hideCompleted } = this.state;

    const { lists } = this.props;

    return lists.map(list => {
      const currentUserId =
        this.props.currentUser && this.props.currentUser._id;
      const showPrivateButton = this.userId === currentUserId;

      return (
        <List
          key={list._id}
          list={list}
          hideCompleted={hideCompleted}
          showPrivateButton={showPrivateButton}
        />
      );
    });
  }

  renderForm() {
    if (this.state.hideForm) {
      return (
        <div className="container">
          {this.props.currentUser ? (
            <form className="new-list" onSubmit={this.addList.bind(this)}>
              <input
                type="text"
                ref="listInput"
                placeholder="Type to add a new list"
              />
            </form>
          ) : null}

          {this.props.currentUser ? (
            <form className="new-task" onSubmit={this.addTask.bind(this)}>
              <div className="row">
                <div className="col-lg-2">
                  <select
                    ref="listSelect"
                    value={this.state.name}
                    placeholder="select the todo list"
                  >
                    {this.props.lists.map(list => (
                      <option key={list._id} value={list._id}>
                        {list.name}
                      </option>
                    ))}
                  </select>
                </div>
                <div className="col-lg-10">
                  <input
                    type="text"
                    ref="taskInput"
                    placeholder="Type to add new tasks"
                  />
                </div>
              </div>
            </form>
          ) : null}
        </div>
      );
    }
  }

  renderSettings() {
    const { settings } = this.props;
    return (
      <div className="container">
        <Setting key={settings.id} settings={settings} />
      </div>
    );
  }

  render() {
    const { ready, incompleteCount } = this.props;

    if (!ready) {
      return <p>Laden</p>;
    }
    if (this.state.hideMenu) {
      return (
        <div className="container">
          <header>
            <h1>
              Home <br />
              To do : {incompleteCount}
            </h1>

            <label className="hide-completed">
              <input
                type="checkbox"
                readOnly
                checked={this.state.hideCompleted}
                onClick={this.toggleHideCompleted.bind(this)}
              />
              Hide Completed Tasks
            </label>
            <button
              className="hideForm"
              onClick={this.toggleHideForm.bind(this)}
            >
              {this.state.hideForm ? "hide form" : "show form"}
            </button>

            <button className="toggleMenu" onClick={this.toggleMenu.bind(this)}>
              {this.state.hideMenu ? "about" : "home"}
            </button>

            <AccountsUIWrapper />
          </header>
          {this.renderForm()}
          {this.renderLists()}
        </div>
      );
    } else {
      return (
        <div className="container">
          <header>
            <h1>About</h1>

            <button className="toggleMenu" onClick={this.toggleMenu.bind(this)}>
              {this.state.hideMenu ? "about" : "home"}
            </button>

            <AccountsUIWrapper />
          </header>
          {this.renderSettings()}
        </div>
      );
    }
  }
}

export default withTracker(() => {
  const sub1 = Meteor.subscribe("tasks");
  const sub2 = Meteor.subscribe("lists");
  const sub3 = Meteor.subscribe("settings");

  const tasks = Tasks.find({}, { sort: { createdAt: -1 } }).fetch();
  const incompleteCount = Tasks.find({ checked: { $ne: true } }).count();
  const currentUser = Meteor.user();
  const lists = Lists.find({}, { sort: { createdAt: -1 } }).fetch();
  const settings = Settings.find({}).fetch();

  return {
    ready: sub1.ready() && sub2.ready() && sub3.ready(),
    tasks,
    incompleteCount,
    currentUser,
    lists,
    settings
  };
})(App);
