import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { check } from "meteor/check";

export const Lists = new Mongo.Collection("lists");

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish lists that are public  or belong to the current user
  Meteor.publish("lists", function listsPublication() {
    return Lists.find({
      $or: [{ private: { $ne: false } }, { owner: this.userId }]
    
    });
  });
}

Meteor.methods({
  "lists.insert"(name) {
    check(name, String);

    // Make sure the user is logged in before inserting a task
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    
    const document = {
      name: name,
      createdAt: new Date(),
      owner: this.userId,
      createdBy: this.userId,
      username: Meteor.users.findOne(this.userId).username
    };
    
    const _id = Lists.insert(document);

    return _id;
  },

  "lists.remove"(listId) {
    check(listId, String);
    const list = Lists.findOne(listId);
    if (list.private && list.owner !== this.userId) {
      // If the task is private, make sure only the owner can delete it
      alert("you are not authorized to do this")
      throw new Meteor.Error("not-authorized");
    }
    Lists.remove(listId);
  },
  
  "lists.setPrivate"(listId, setToPrivate) {
    check(listId, String);
    check(setToPrivate, Boolean);
    const list = Lists.findOne(listId);

    // Make sure only the list owner can make a list private
    if (list.owner !== this.userId) {
      alert("you are not authorized to do this")
      throw new Meteor.Error("not-authorized");
   }
    Lists.update(listId, { $set: { private: setToPrivate } });
 
  }
});
