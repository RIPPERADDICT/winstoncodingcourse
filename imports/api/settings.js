import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { check } from "meteor/check";

export const Settings = new Mongo.Collection("settings");

if (Meteor.isServer) {
    // This code only runs on the server
    // Only publish lists that are public  or belong to the current user
    Meteor.publish("settings", function settingsPublication() {
      return Settings.find({
        $or: [{ private: { $ne: false } }, { owner: this.userId }]
      });
    });
}

Meteor.methods({
    "settings.insert"(text){
      check(text, String)
 
      const document = {
        text,
        createdAt: new Date(),
        owner: this.userId,
      };

      const _id = Settings.insert(document);
      return _id;
    }
});